from kivy.app import App
from kivy.uix.gridlayout import GridLayout
from kivy.uix.label import Label
from kivy.uix.button import Button
from kivy.uix.textinput import TextInput



class Texte(Label):
    def __init__(self, **kwargs):
        Label.__init__(self, **kwargs)
        self.bind(size=self.set_size)

    def set_size(self, widget, value):
        widget.text_size = self.size


class Bouton(Button):
    def __init__(self, color="red", **kwargs):
        Button.__init__(self, color=color, **kwargs)


class Fenetre(GridLayout):
    def __init__(self, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)

    def afficher_menu(self, menu):
        self.clear_widgets()
        self.add_widget(menu)



class Menus():
    def __init__(self, **kwargs):
        self.fenetre = Fenetre()
        self.liaison = Liaison(self)
        self.menu_images = MenuImages(self)

        self.fenetre.afficher_menu(self.liaison)

    def afficher_menu(self, menu):
        self.fenetre.afficher_menu(menu)


class Liaison(GridLayout):
    def __init__(self, menus):
        GridLayout.__init__(self, cols=1, rows=1)
        self.window=GridLayout(cols=2, rows=4)
        self.add_widget(self.window)
        self.menus = menus
        self.execution()


    def execution(self):
        self.textInput_label = Label(text="Saisisez votre nom :")
        self.window.add_widget(self.textInput_label)
        self.textInput=TextInput( multiline=False)
        self.window.add_widget(self.textInput)

        self.textInput.bind(text=self.changer_textInput)

        boutton_Rouge=Button(text="red")
        boutton_Rouge.bind(on_press=self.my_color_Rouge)
        self.window.add_widget(boutton_Rouge)

        boutton_Bleu=Button(text="blue")
        boutton_Bleu.bind(on_press=self.my_color_Bleu)
        self.window.add_widget(boutton_Bleu)

        boutton_green=Button(text="green")
        boutton_green.bind(on_press=self.my_color_green)
        self.window.add_widget(boutton_green)

        boutton_yellow =Button(text="yellow")
        boutton_yellow.bind(on_press=self.my_color_yellow)
        self.window.add_widget(boutton_yellow)

        boutton_pink =Button(text="pink")
        boutton_pink.bind(on_press=self.my_color_pink)
        self.window.add_widget(boutton_pink)

        boutton_brown =Button(text="brown")
        boutton_brown.bind(on_press=self.my_color_brown)
        self.window.add_widget(boutton_brown)

    def my_color_Rouge(self, widget):
        self.textInput_label.color = "red"

    def my_color_Bleu(self, widget):
        self.textInput_label.color = "blue"

    def my_color_Vert(self, widget):
        self.textInput_label.color ="vert"

    def my_color_yellow(self, widget):
        self.textInput_label.color = "yellow"

    def my_color_pink(self, widget):
        self.textInput_label.color = "pink"

    def my_color_brown(self, widget):
        self.textInput_label.color = "brown"

    def my_color_green(self, widget):
        self.textInput_label.color = "green"

    def changer_textInput(self, widget, valeur):

        print("valeur", valeur)
        self.textInput_label.text=self.textInput.text
        #print("valeur", self.textInput_label.text)


class MenuImages(GridLayout):
    def __init__(self, menus, **kwargs):
        GridLayout.__init__(self, cols=1, rows=1, **kwargs)
        self.menus = menus
        self.grid1 = GridLayout(cols=1, rows=1)
        self.add_widget(self.grid1)
        self.layout1()


    def layout1(self):
        b1 = Bouton(text="Quitter")
        b1.bind(on_press=self.clbk_menu_connexion)
        self.grid1.add_widget(b1)

    def clbk_menu_connexion(self, widget):
        self.menus.afficher_menu(self.menus.menu_connexion)



class MyApp(App):

    def build(self):
        menus = Menus()
        return menus.fenetre



if __name__ == "__main__":
    MyApp().run()
